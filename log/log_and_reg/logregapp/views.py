from django.shortcuts import render, HttpResponse, redirect
from django.contrib import messages
from . models import *

def index(request):
    return render(request, "log.html")

def success(request):
    context = {
        'all_messages': Wall_Message.objects.all()
    }
    return render(request, "success.html", context)

def register(request):
    if request.method == 'POST':
        errors = User.objects.validator(request.POST)
        print(errors)
        if len(errors) > 0:
            for key, values in errors.items():
                messages.error(request, values)
            return redirect('/')
        logged_user = User.objects.create(first_name=request.POST['first_name'], last_name=request.POST['last_name'], email=request.POST['email'], password=request.POST['password'])
        request.session['name'] = logged_user.first_name
        request.session['user_id'] = logged_user.id
        return redirect('/success')
    return redirect('/')

def login(request):
    print("is my method being called?")
    if request.method == 'POST':
        print("is it a post request?")
        logged_user = User.objects.filter(email=request.POST['email'])
        if len(logged_user) > 0:
            logged_user = logged_user[0]
            print(logged_user)
            print(logged_user.password, request.POST['password'])
            if logged_user.password == request.POST['password']:
                request.session['name'] = logged_user.first_name
                request.session['user_id'] = logged_user.id
                return redirect('/success')
    return redirect('/')

def logout(request):
    request.session.flush()
    return redirect('/')

def add_message(request):
    if request.method == 'POST':
        #errors = Wall_Message.objects.validator(request.POST)
        #if errors:
        #    for key, values in errors.items():
        #        messages.error(request, values)
        #    return redirect('/success')
        print(request.session['user_id'])
        new_mess = Wall_Message.objects.create(content=request.POST['content'], poster=User.objects.get(id=request.session['user_id']))
        print(new_mess)
        return redirect('/success')
    return redirect('/')

def add_comment(request, id):
    poster = User.objects.get(id=request.session['user_id'])
    mess = Wall_Message.objects.get(id=id)
    Comment.objects.create(content = request.POST['content'], poster=poster, message=mess)
    return redirect('/success')


def profile(request, id):
    context = {
        'one_user': User.objects.get(id=id)
    }
    return render(request, 'profile.html', context)

def edit_mess(request, id):
    one_mess = Wall_Message.objects.get(id=id)
    if request.method == 'POST':
        one_mess.content = request.POST['content']
        one_mess.save()
        return redirect(f'/profile/{str(one_mess.poster.id)}')
    context = {
        'edit_mess': one_mess
    }
    return render(request, 'edit.html', context)

def delete_mess(request, id):
    Wall_Message.objects.get(id=id).delete()
    return redirect('/success')

def add_like(request, id):
    liked_message = Wall_Message.objects.get(id=id)
    user_liking = User.objects.get(id=request.session['user_id'])
    liked_message.likes.add(user_liking)
    return redirect('/success')

