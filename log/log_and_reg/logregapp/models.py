from django.db import models
import re 

class UserManager(models.Manager):
    def validator(self, postData):
        errors = {}
        EMAIL_REGEX = re.compile(r'^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$')
        if len(postData['first_name']) <2:
            errors['first_name'] = "Your first name must be more than two chars"
        if len(postData['last_name']) <2:
            errors['last_name'] = "Your last name must be more than two chars"
        if not EMAIL_REGEX.match(postData['email']):          
            errors['email'] = "Invalid email address!"
        if len(postData['password']) <8:
            errors['password'] = "password must be at least 8 chars"
        if postData['password'] != postData['confirm_password']:
            errors['confirm_password'] = "must confirm password"
        return errors

class User(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = UserManager()

class Wall_MessageManager(models.Model):
    def validator(self, form):
        errors = {}
        print("validator")
        if len(form['content']) < 5:
            errors['length'] = "message must be at least 5 characters"
        return errors

class Wall_Message(models.Model):
    content = models.CharField(max_length=100)
    poster = models.ForeignKey(User, related_name="messages", on_delete=models.CASCADE)
    likes = models.ManyToManyField(User, related_name="likes")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = Wall_MessageManager()

class Comment(models.Model):
    content = models.CharField(max_length=255)
    poster = models.ForeignKey(User, related_name="comments", on_delete=models.CASCADE)
    message = models.ForeignKey(Wall_Message, related_name="comments", on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)