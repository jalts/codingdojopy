from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('', include('single_model_orm.urls')),
]
