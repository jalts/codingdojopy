# Generated by Django 3.1 on 2020-08-09 23:40

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('single_model_orm', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='wizard',
            name='created_at',
        ),
        migrations.RemoveField(
            model_name='wizard',
            name='updated_at',
        ),
    ]
