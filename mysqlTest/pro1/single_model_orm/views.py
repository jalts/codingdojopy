from django.shortcuts import render, redirect
from .models import Wizard

# Create your views here.
def index(request):
    context = {
        'Wizard': Wizard.objects.all()
    }
    return render(request, 'index.html', context)

def create(request):
    Wizard.objects.create(
        first_name=request.POST['first_name'],
        last_name=request.POST['last_name'],
        email=request.POST['email'],
        age=request.POST['age']
    )
    return redirect('/')