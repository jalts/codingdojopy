from django.apps import AppConfig


class SingleModelOrmConfig(AppConfig):
    name = 'single_model_orm'
