from django.db import models

class Wizard(models.Model):
    first_name = models.CharField(max_length=45)
    last_name = models.CharField(max_length=45)
    email_address = models.CharField(max_length=45)
    age = models.IntegerField()

    @property
    def full_name(self):
        return f"{self.first_name} {self.last_name}"