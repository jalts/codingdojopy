from django.urls import path, include
from tvapp import views

urlpatterns = [
    path('', views.index),
    path('shows/', include('tvapp.urls')),
]
