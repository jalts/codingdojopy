class BankAccount:
    def __init__(self, int_rate=0, balance=0):
        self.int_rate = int_rate 
        self.balance = balance 
        
    def deposit(self, amount):
        self.balance += amount 
        return self

    def withdraw(self, amount):
        # check the balance and see if it's greater than the amount 
        if self.balance >= amount:
            self.balance -= amount 
        else:
            self.balance -= 5 
            print("Insufficient Funds: Charging a $5 fee")
        return self

    def display_account_info(self):
        print(f"Balance: ${self.balance}")
        return self

    def yield_interest(self):
        if self.balance > 0:
            self.balance = self.balance + self.balance * self.int_rate
        return self

class User:
    def __init__(self, username, email_address, BankAccount):
        self.name = username
        self.email = email_address
        self.account = BankAccount(int_rate=0.02, balance=0)
    def make_deposit(self, amount):
        self.account += amount
    def make_withdrawal(self, amount):
        self.account -= amount
    def display_user_balance(self):
        print(f"Hello {self.name}! Turns out, you have {self.account} left!")
    def transfer_money(self, other_user, amount):
        other_user.make_deposit(amount)
        self.make_withdrawal(amount)
        print(f"{self.name} transferred money!")

james = User("James Altheide", "james@gmail.com", 'ba1')
alex = User("Alex Altheide", "alex@gmail.com", 'ba2')
kyle = User("Kyle Perlman", "kyle@gmail.com", 'ba3')

james.account.deposit(10000).account.withdraw(2000).account.deposit(2000)

print(james.name)
print(alex.name)
print(james.account)
james.display_user_balance()

james.transfer_money(alex, 5000)
james.display_user_balance()
alex.display_user_balance()

ba1 = BankAccount(balance=100)
ba2 = BankAccount(.01, 500)

ba1.deposit(100).deposit(50).deposit(50).withdraw(100).yield_interest().display_account_info()
ba2.deposit(100).deposit(100).withdraw(50).withdraw(50).withdraw(50).withdraw(50).yield_interest().display_account_info()


""" ba1.deposit(100).deposit(50).deposit(50).withdraw(100).yield_interest().display_account_info()
ba2.deposit(100).deposit(100).withdraw(50).withdraw(50).withdraw(50).withdraw(50).yield_interest().display_account_info() """