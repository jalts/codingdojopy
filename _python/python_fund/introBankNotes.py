""" 
Over the next few tabs, we'll start fleshing out our User class with:

Attributes: Characteristics shared by all instances of the class type.
Methods: Actions that an object can perform. A user, for example, should be able to make a deposit or a withdrawal, or maybe send money to another user. """





# Attributes

class User:
    def __init__(self, username, email_address):
        self.name = username
        self.email = email_address
        self.account_balance = 0
    def make_deposit(self, amount):
        self.account_balance += amount
    def make_withdrawal(self, amount):
        self.account_balance -= amount
    def display_user_balance(self):
        print(f"Hello {self.name}! Turns out, you have {self.account_balance} left!")
    def transfer_money(self, other_user, amount):
        other_user.make_deposit(amount)
        self.make_withdrawal(amount)
        print(f"{self.name} transferred money!")

james = User("James Altheide", "james@gmail.com")
alex = User("Alex Altheide", "alex@gmail.com")
kyle = User("Kyle Perlman", "kyle@gmail.com")


james.make_deposit(10000).make_withdrawal(2000).make_deposit(2000)


print(james.name)
print(alex.name)
print(james.account_balance)
james.display_user_balance()

james.transfer_money(alex, 5000)
james.display_user_balance()
alex.display_user_balance()






# make_withdrawal(self, amount) - have this method decrease the user's balance by the amount specified
# display_user_balance(self) - have this method print the user's name and account balance to the terminal - eg. "User: Guido van Rossum, Balance: $150

# BONUS: transfer_money(self, other_user, amount) - have this method decrease the user's balance by the amount and add that amount to other other_user's balance
